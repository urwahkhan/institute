/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package institute;

import java.util.List;

/**
 *
 * @author urwah
 */
public class Department {
    private String name;
    private List<Student> students;
    
    public Department(String name,List<Student> students){
        this.name=name;
        this.students=students;
        System.out.println("Department name:"+name);
        System.out.println("Students:"+students);
    }
    
    public String getName(){
        return this.name;
    }
    public List<Student> getStudents(){
        return this.students;
    }
    
    
}

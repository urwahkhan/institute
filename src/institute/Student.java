/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package institute;
import java.io.*;
import java.util.*;

/**
 *
 * @author urwah
 */
public class Student {
    private String name;
    private int id;
    
    public Student(String name, int id){
        this.name=name;
        this.id=id;
        System.out.println("Student name:"+name+"  "+ "Student ID:"+id);
        
    }
    
    public String getName(){
        return this.name;
    }
    public int getId(){
        return this.id;
    }
    
}

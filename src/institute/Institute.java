/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package institute;

import java.util.List;

/**
 *
 * @author urwah
 */
public class Institute {

   private String name;
   private List<Department> department;
   
   public Institute(String name, List<Department> department){
       this.name=name;
       this.department=department;
       
        System.out.println("Institute name:"+name);
        System.out.println("Department:"+department);
   }
   public String getName(){
       return this.name;
   }
   public List<Department>getDepartments(){
       return this.department;
   }
}

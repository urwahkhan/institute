/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package institute;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author urwah
 */
public class Test {
    
    public static void main(String[]args){
    Student s1 = new Student("Urwah Khan",99163);
    Student s2 = new Student("Aisha Habra",78821);
    Student s3 = new Student("Rafah Sawadi", 55783);
    Student s4 = new Student("Fatima Baraghid",72234);
    //list of medical students
    List<Student> medicine = new ArrayList<Student>();
    medicine.add(s3);
    medicine.add(s4);
    //list of computer science students
    List<Student> CS = new ArrayList<Student>();
    CS.add(s1);
    CS.add(s2);
    
    Department ComS = new Department("Computer Science",CS);
    Department med = new Department("Medicine",medicine);
    
    //list of departments
    List<Department> departments= new ArrayList<Department>();
    departments.add(ComS);
    departments.add(med);
    // instance of departments
    Institute ins=new Institute("Istanbul university",departments);
    
    System.out.println(ins);
    System.out.println();
    
    
    
    }
    
}

package institute;


import institute.Books;
import java.util.ArrayList;
import java.util.List;

public class TestLibrary {
    public static void main(String[]args){
        Books b1=new Books("The fault in our stars","John Green");
        Books b2=new Books("The maze runner","James Dashner");
        Books b3=new Books("The fault in our stars","Tim Ferris");
        Books b4=new Books("Diwan-e-Ghalib","Mirza Ghalib");
        List<Books> book= new ArrayList<Books>();
        book.add(b1);
        book.add(b2);
        book.add(b3);
        book.add(b4);
        
Library library = new Library(book);
List<Books>bks = library.getBooks();
for (Books bk :bks){
    System.out.println("Title: "+ bk.title+ " and "+ "Author: "+bk.author);
}
        
    }
}
